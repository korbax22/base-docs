*********************
6. Working with git
*********************

Git Workflow
=====================

Branch
---------------------
* Turn on to master branch
* Pulling all last changes from remout repo
* Create new branch from master
* Use number of task when you make branch 

::

    git checkout master
    git pull
    git checkout -b branch_number-of-task origin/master

.. important:: Important, make sure that on live server set active master branch

Commit
---------------------
* Use for name commit number and title of the task
* Before push your changes to server, pulling changes from remout server
::

    git commit -m"#number-of-task_caption-task"
    git pull origin branch_number-of-task
    git push origin branch_number-of-task

Live server
=====================

| Branch is active - master
| For add changes on live server merge your branch to master branch
| After merge, delete your branch

Dev server
=====================

| Branch is active - dev
| For add changes on dev server merge your branch to dev branch

.. note::
    More info about working with git, you can find on Magecom portal: https://portal.magecom.net

